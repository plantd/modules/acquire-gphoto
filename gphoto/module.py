import gi

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex
from gi.repository import GLib

from gphoto.model import AppModel
from gphoto.job import ConnectJob, DisconnectJob, ReconnectJob, CaptureJob, StartJob, StopJob

#pylint: enable=wrong-import-position

class App(Apex.Application):
    """App class is the module runner"""
    __gtype_name__ = "App"

    def __init__(self, endpoint, service, config, *args, **kwargs):
        super().__init__(*args,
                         application_id="org.plantd.acquire.GPhoto",
                         flags=0,
                         **kwargs)
        self.set_endpoint(endpoint)
        self.set_service(service)
        self.set_inactivity_timeout(10000)
        self.load_config(config)
        self.props = []
        self.model = AppModel()
        self.model.connect("changed", self.on_model_changed)
        self.__setup_properties()

    def __setup_properties(self):
        for key, value in self.model.props.items():
            prop = Apex.Property.new(key, str(value))
            self.props.append(prop)
            self.add_property(prop)
            if self.has_property(key):
                Apex.debug("added %s property" % key)

    def do_activate(self):
        # Connect camera?
        pass

    def do_shutdown(self):
        # Disconnect camera?
        pass

    def on_model_changed(self, *args):
        """Callback received when the data model changes"""
        Apex.debug("model change (%s: %s)" % (args[1], args[2]))
        self.update_property(args[1], args[2])

    # apex_application_get_configuration override
    def do_get_configuration(self):
        """Handles the `get-configuration' message."""
        Apex.debug("get-configuration")
        configuration = self.get_loaded_config()
        response = Apex.ConfigurationResponse.new()
        response.set_configuration(configuration)
        return response

    # apex_application_get_status override
    # TODO: add the actual status
    def do_get_status(self):
        """Handles the `get-status' message."""
        Apex.debug("get-status")
        response = Apex.StatusResponse.new()
        return response

    # apex_application_submit_job override
    def do_submit_job(self, job_name, job_value, job_properties):
        """Handles the `submit-job' message."""
        Apex.info("submit-job: %s [%s]" % (job_name, job_value))
        response = Apex.JobResponse.new()
        if job_name == "connect":
            job = ConnectJob(self.model)
            # job.connect("event", self.on_publish_event)
            response.set_job(job)
        elif job_name == "disconnect":
            job = DisconnectJob(self.model)
            # job.connect("event", self.on_publish_event)
            response.set_job(job)
        elif job_name == "reconnect":
            job = ReconnectJob(self.model)
            # job.connect("event", self.on_publish_event)
            response.set_job(job)
        elif job_name == "capture":
            job = CaptureJob(self.model)
            # job.connect("event", self.on_publish_event)
            response.set_job(job)
        elif job_name == "start-acquiring":
            self.model.set("interval", job_properties["interval"])
            self.model.set("total-images", job_properties["count"])
            job = StartJob(self.model)
            # job.connect("event", self.on_publish_event)
            response.set_job(job)
        elif job_name == "stop-acquiring":
            job = StopJob(self.model)
            # job.connect("event", self.on_publish_event)
            response.set_job(job)
        else:
            pass
        return response
