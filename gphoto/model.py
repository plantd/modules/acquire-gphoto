import gi
import os

import gphoto2 as gp

#pylint: disable=wrong-import-position

from gi.repository import GObject, GLib

#pylint: enable=wrong-import-position

class AppModel(GObject.Object):

    __gsignals__ = {
        'changed': (GObject.SIGNAL_RUN_LAST, None, (str,str,))
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.camera = gp.Camera()
        # Property backing fields
        self.props = {}
        self.props["total-images"] = 0
        self.props["image-count"] = 0
        self.props["interval"] = 0
        self.props["connected"] = False
        self.props["acquiring"] = False
        self.props["image-location"] = "http://localhost/images/latest.jpg"
        self.props["name"] = os.getenv("PLANTD_MODULE_SERVICE", "acquire-gphoto")

    def update(self, key, value):
        if key in self.props:
            if type(self.props[key]) == int:
                self.props.update({key: int(value)})
            elif type(self.props[key]) == float:
                self.props.update({key: float(value)})
            elif type(self.props[key] == str):
                self.props.update({key: value})
            elif type(self.props[key] == bool):
                self.props.update({key: value})

    def set(self, key, value):
        if key in self.props:
            if type(self.props[key]) == int:
                self.props.update({key: int(value)})
            elif type(self.props[key]) == float:
                self.props.update({key: float(value)})
            elif type(self.props[key] == str):
                self.props.update({key: value})
            elif type(self.props[key] == bool):
                self.props.update({key: value})
            self.emit("changed", key, str(value))

    def get(self, key):
        if key in self.props:
            return str(self.props.get(key))
        else:
            return None
