import gi

#pylint: disable=wrong-import-position

from gi.repository import GLib

#pylint: enable=wrong-import-position

def get_tz_timestamp():
    datetime = GLib.DateTime.new_now_local()
    year = datetime.get_year()
    mon = datetime.get_month()
    day = datetime.get_day_of_month()
    hour = datetime.get_hour()
    minute = datetime.get_minute()
    second = datetime.get_second()
    microsecond = datetime.get_microsecond()
    timezone = datetime.format("%z")
    timestamp = '{}-{:02d}-{:02d}_{:02d}-{:02d}-{:02d}.{}{}'.format(
        year, mon, day, hour, minute, second, microsecond, timezone)
    return timestamp
