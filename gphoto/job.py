import os
import time
import shutil
import gi

import gphoto2 as gp

from gphoto import util

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex
from gi.repository import GLib, GObject

#pylint: enable=wrong-import-position

class ConnectJob(Apex.Job):
    """Provides a task handler that connects the camera device.

    Args:
        model (AppModel): Application data model for properties and camera device.

    Attributes:
        model (AppModel): Application data model for properties and camera device.

    """

    __gtype_name__ = "ConnectJob"

    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,))
    }

    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model

    def do_task(self):
        Apex.debug("connect the camera")
        if not self.model.props["connected"]:
            self.model.camera.init()
            self.model.set("connected", True)


class DisconnectJob(Apex.Job):
    """Provides a task handler that disconnects the camera device.

    Args:
        model (AppModel): Application data model for properties and camera device.

    Attributes:
        model (AppModel): Application data model for properties and camera device.

    """

    __gtype_name__ = "DisconnectJob"

    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,))
    }

    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model

    def do_task(self):
        Apex.debug("disconnect the camera")
        if self.model.props["connected"]:
            self.model.camera.exit()
            self.model.set("connected", False)


class ReconnectJob(Apex.Job):
    """Provides a task handler that reconnects the camera device.

    Args:
        model (AppModel): Application data model for properties and camera device.

    Attributes:
        model (AppModel): Application data model for properties and camera device.

    """

    __gtype_name__ = "ReconnectJob"

    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,))
    }

    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model

    def do_task(self):
        Apex.debug("reconnect the camera")
        if self.model.props["connected"]:
            self.model.camera.exit()
            time.sleep(1)
            self.model.camera.init()
            self.model.set("connected", True)


class CaptureJob(Apex.Job):
    """Provides a task handler that captures a single image.

    Args:
        model (AppModel): Application data model for properties and camera device.

    Attributes:
        model (AppModel): Application data model for properties and camera device.

    """

    __gtype_name__ = "CaptureJob"

    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,))
    }

    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model

    def do_task(self):
        Apex.debug("capture single image")
        if self.model.props["connected"]:
            path = os.getenv("PLANTD_MODULE_GPHOTO_OUTPUT", "/tmp/images")
            file_path = self.model.camera.capture(gp.GP_CAPTURE_IMAGE)
            _, extension = os.path.splitext(file_path.name)
            timestamp = util.get_tz_timestamp()
            filename = timestamp + extension
            target = os.path.join(path, filename)
            camera_file = self.model.camera.file_get(file_path.folder,
                                                     file_path.name,
                                                     gp.GP_FILE_TYPE_NORMAL)
            camera_file.save(target)

            # Copy the file over as eg. "latest.jpg"
            latest = "latest" + extension
            dst = os.path.join(path, latest)
            try:
                shutil.copy2(target, dst)
            except FileNotFoundError:
                Apex.warning("Failed to copy:", filename)


class StartJob(Apex.Job):
    """Provides a task handler that starts performing time-lapse acquisition.

    Args:
        model (AppModel): Application data model for properties and camera device.

    Attributes:
        model (AppModel): Application data model for properties and camera device.

    """

    __gtype_name__ = "StartJob"

    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,))
    }

    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model
        self.path = os.getenv("PLANTD_MODULE_GPHOTO_OUTPUT", "/tmp/images")

    def do_task(self):
        Apex.debug("start time-lapse acquisition")
        if self.model.props["connected"] and not self.model.props["acquiring"]:
            self.model.set("image-count", 0)
            self.model.set("acquiring", True)
            GLib.timeout_add_seconds(self.model.props["interval"], self.on_interval_timeout, None)

    def on_interval_timeout(self, user_data):
        file_path = self.model.camera.capture(gp.GP_CAPTURE_IMAGE)
        camera_file = self.model.camera.file_get(file_path.folder,
                                                 file_path.name,
                                                 gp.GP_FILE_TYPE_NORMAL)
        _, extension = os.path.splitext(file_path.name)
        timestamp = util.get_tz_timestamp()
        filename = timestamp + extension
        target = os.path.join(self.path, filename)
        camera_file.save(target)
        self.model.set("image-count", self.model.props["image-count"] + 1)
        location = os.path.join(os.getenv("PLANTD_MODULE_GPHOTO_LOCATION", "http://localhost/images"), filename)
        self.model.set("image-location", location)

        # Copy the file over as eg. "latest.jpg"
        latest = "latest" + extension
        dst = os.path.join(self.path, latest)
        try:
            shutil.copy2(target, dst)
        except FileNotFoundError:
            Apex.warning("Failed to copy:", filename)

        # Stop when the image count is reached
        if self.model.props["image-count"] >= self.model.props["total-images"]:
            self.model.set("acquiring", False)
            return False

        # FIXME: this only stops after the next interval
        if not self.model.props["acquiring"]:
            self.model.set("acquiring", False)
            return False

        return True


class StopJob(Apex.Job):
    """Provides a task handler that stops performing time-lapse acquisition.

    Args:
        model (AppModel): Application data model for properties and camera device.

    Attributes:
        model (AppModel): Application data model for properties and camera device.

    """

    __gtype_name__ = "StopJob"

    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,))
    }

    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model

    # FIXME: this only stops after the next interval
    def do_task(self):
        Apex.debug("stop time-lapse acquisition")
        if self.model.props["connected"] and self.model.props["acquiring"]:
            self.model.set("acquiring", False)
