from setuptools import setup, find_packages

setup(
    name='acquire-gphoto',
    use_scm_version=True,
    author='Geoff Johnson',
    author_email='geoff.jay@gmail.com',
    description='plantd module for acquiring image data',
    packages=find_packages(),
)
