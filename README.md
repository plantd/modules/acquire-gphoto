# Plantd GPhoto Module

Used to acquire images from a camera using GPhoto.

## Running

### Docker

#### Images

* amd64:
  | `docker pull registry.gitlab.com/plantd/modules/acquire-gphoto:v1`
* arm32v7:
  | `docker pull registry.gitlab.com/plantd/modules/acquire-gphoto/arm32v7:v1`

#### Running

Depending on the image pulled, starting a container will be similar to this.

```sh
docker run --rm -it --privileged \
  -v /dev/bus/usb:/dev/bus/usb \
  -v /tmp/images:/images \
  registry.gitlab.com/plantd/modules/acquire-gphoto:v1
```
